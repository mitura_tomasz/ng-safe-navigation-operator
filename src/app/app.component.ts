import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Player } from './models/player';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  player: Player = { 'name': "max", 'surname': 'mad' };

  constructor() {}
  
  ngOnInit() {
  }
}
